package space.enthropy.norest_proj;


import lombok.*;

@ToString
@NoArgsConstructor
@Setter
@Getter
public class PersonModel {
    private String first_name;
    private String last_name;
    private int age;
    private String email;
    private String token;
}
