package space.enthropy.norest_proj;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

@WebServlet(name = "registrationServlet", value = "/sign-in")
public class RegistrationServlet extends HttpServlet {
    ArrayList<PersonModel> list = new ArrayList<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

//        for (PersonModel p : list) {
//            if (p.getToken().equals(Arrays.stream(req.getCookies())
//                    .toList().stream().filter(x -> x.getName().equals("access_token"))
//                    .collect(Collectors.toCollection(ArrayList::new)).get(0).getName())) {
//                PrintWriter pw = new PrintWriter(resp.getWriter());
//                pw.println(p);
//                pw.close();
//            }
//        }
        PersonModel person = new PersonModel();
        person.setFirst_name(req.getParameter("first_name"));
        person.setLast_name(req.getParameter("last_name"));
        person.setAge(Integer.parseInt(req.getParameter("age")));
        person.setEmail(req.getParameter("email"));

        if (Arrays.toString(req.getCookies()).contains("access_token")) {
            if (Arrays.stream(req.getCookies())
                    .toList().stream().filter(x -> x.getName().equals("access_token"))
                    .collect(Collectors.toCollection(ArrayList::new)).get(0).getName().equals("access_token")) {
                for (PersonModel p : list) {
                    if (p.getToken().equals(Arrays.stream(req.getCookies())
                            .toList().stream().filter(x -> x.getName().equals("access_token"))
                            .collect(Collectors.toCollection(ArrayList::new)).get(0).getName())) {
                        PrintWriter pw = new PrintWriter(resp.getWriter());
                        pw.println(p);
                        pw.close();
                    }
                }
            }
        } else {
            Cookie cookie = new Cookie("access_token", UUID.randomUUID().toString());
            person.setToken(cookie.getValue());
            list.add(person);
            cookie.setMaxAge(60);
            cookie.setComment("This cookie is for registered person");
            resp.addCookie(cookie);
            PrintWriter pw = new PrintWriter(resp.getWriter());
            pw.println(person);
            pw.close();
        }
    }
}
