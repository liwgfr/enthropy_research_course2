package space.enthropy.norest_proj;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.UUID;

@WebServlet("/add-cookie")
public class CookiesServlet extends HttpServlet {
    // Cookie - это временные файлы (ключ-значение), которые хранятся у клиента.

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        String value = UUID.randomUUID().toString().replaceAll("-", "");
        Cookie cookie = new Cookie("token", value);
        cookie.setMaxAge(-1); // -1 -> Session
        cookie.setMaxAge(60); // in seconds
        resp.addCookie(cookie);
        PrintWriter pw = new PrintWriter(resp.getWriter());
        pw.println("Cookie is set!");
        pw.close();
    }
}
