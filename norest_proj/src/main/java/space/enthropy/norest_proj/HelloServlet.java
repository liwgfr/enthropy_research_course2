package space.enthropy.norest_proj;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "helloServlet", value = "/hello")
public class HelloServlet extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    // GET - что-то получить - Consumes()
    // POST - отправить или обновить - Consumes + Produces

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html"); // MIME тип

        PrintWriter out = response.getWriter();
        String input1 = request.getParameter("x"); // Query Param
        String input2 = request.getParameter("y"); // Query Param
        double result;
        try {
            result = Double.parseDouble(input1) / Double.parseDouble(input2);
        } catch (ArithmeticException e) { // ArithmeticException только если int / int (1 / 0). double / double (1.0 / 0.0) -> Infinity
            result = -111.0;
        }
        String[] ar = request.getParameterValues("vals"); // Принимаем массив
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
        out.println("<h3>То, что мы ввели: " + result + "</h3>");
        out.println("<h2>Array: ");
        for (String s : ar) {
            out.println(s + " "); // выводим данные из массива, которые приняли в query param
        }
        out.println("</h2>");
    }

    public void destroy() {

    }
}